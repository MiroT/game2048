﻿using System.Collections.Generic;

namespace ProjectGame2048Tests
{
    internal static class TestUtilities
    {
        public static List<int> GetAllNumbersInGameFieldMatrix(int[,] matrix)
        {
            List<int> listOfNumbers = new List<int>();

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    listOfNumbers.Add(matrix[row, col]);
                }
            }

            return listOfNumbers;
        }
    }
}
