﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGame2048BLL.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame2048Tests
{
    [TestClass]
    public class EndOfGameTests
    {
        public int[,] MatrixTest_1 { get; private set; }
        public int[,] MatrixTest_2 { get; private set; }

        [TestInitialize]
        public void InitalizeMatrix()
        {
            //Arrange
            MatrixTest_1 = new int[,]
            {
                { 4, 2, 2, 4 },
                { 8, 4, 32, 4 },
                { 4, 4, 8, 4 },
                { 4, 8, 8, 0 }
            };

            MatrixTest_2 = new int[,]
            {
                { 4, 16, 2, 4 },
                { 8, 4, 32, 8 },
                { 16, 2, 8, 4 },
                { 4, 8, 64, 2 }
            };
        }

        [TestMethod]
        public void CheckForFreePositionWithFreePositionTest()
        {
            //Act
            bool result = Movement.CheckForFreePosition(this.MatrixTest_1);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckForFreePositionWithoutFreePositionTest()
        {
            //Act
            bool result = Movement.CheckForFreePosition(this.MatrixTest_2);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CheckIfSumIsPossibeWithPossibleSumTest()
        {
            //Act
            bool result = Movement.CheckIfSumIsPossibe(this.MatrixTest_1);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckIfSumIsPossibeWithoutPossibleSumTest()
        {
            //Act
            bool result = Movement.CheckIfSumIsPossibe(this.MatrixTest_2);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
