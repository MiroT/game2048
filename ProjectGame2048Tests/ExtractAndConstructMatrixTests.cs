﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGame2048BLL.Engine;

namespace ProjectGame2048Tests
{
    [TestClass]
    public class ExtractAndConstructMatrixTests
    {
        public int[,] MatrixTest { get; private set; }

        [TestInitialize]
        public void InitializeMatrix()
        {
            //Arrange
            MatrixTest = new int[4, 4]
            {
              { 2, 16, 8, 4 },
              { 4, 0, 4, 2 },
              { 16, 16, 4, 32 },
              { 8, 2, 4, 64 }
            };
        }

        [TestMethod]
        public void ExtractColumnsFromMatrixTest()
        {
            //Arrange
            int[][] jarrArrayResult = new int[][]
            {
              new int[] { 2, 4, 16, 8 },
              new int[] { 16, 0, 16, 2 },
              new int[] { 8, 4, 4, 4 },
              new int[] { 4, 2, 32, 64 }
            };

            //Act
            int[][] jarrArrayExtracted = Movement.ExtractColumnsFromMatrix(this.MatrixTest);

            //Assert
            for (int i = 0; i < 4; i++)
            {
                CollectionAssert.AreEqual(jarrArrayResult[i], jarrArrayExtracted[i]);
            }
        }

        [TestMethod]
        public void ExtractRowsFromMatrixTest()
        {
            //Arrange
            int[][] jarrArrayResult = new int[][]
            {
              new int[] { 2, 16, 8, 4 },
              new int[] { 4, 0, 4, 2 },
              new int[] { 16, 16, 4, 32 },
              new int[] { 8, 2, 4, 64 }
            };

            //Act
            int[][] jarrArrayExtracted = Movement.ExtractRowsFromMatrix(this.MatrixTest);

            //Assert
            for (int i = 0; i < 4; i++)
            {
                CollectionAssert.AreEqual(jarrArrayResult[i], jarrArrayExtracted[i]);
            }
        }

        [TestMethod]
        public void ConstructAMatrixFromArraysColumnsTest()
        {
            //Arrange
            int[][] jarrArray = new int[][]
            {
              new int[] { 2, 4, 16, 8 },
              new int[] { 16, 0, 16, 2 },
              new int[] { 8, 4, 4, 4 },
              new int[] { 4, 2, 32, 64 }
            };

            //Act
            int[,] matrixExtracted = Movement.ConstructAMatrixFromArraysColumns(jarrArray);

            //Assert
            CollectionAssert.AreEqual(this.MatrixTest, matrixExtracted);
        }

        [TestMethod]
        public void ConstructAMatrixFromArraysRowsTest()
        {
            //Arrange
            int[][] jarrArray = new int[][]
            {
              new int[] { 2, 16, 8, 4 },
              new int[] { 4, 0, 4, 2 },
              new int[] { 16, 16, 4, 32 },
              new int[] { 8, 2, 4, 64 }
            };

            //Act
            int[,] matrixExtracted = Movement.ConstructAMatrixFromArraysRows(jarrArray);

            //Assert
            CollectionAssert.AreEqual(this.MatrixTest, matrixExtracted);
        }
    }
}
