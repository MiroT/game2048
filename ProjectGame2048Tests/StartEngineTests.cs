using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGame2048.Models;
using ProjectGame2048BLL.Engine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectGame2048Tests
{
    [TestClass]
    public class StartEngineTests
    {
        [TestMethod]
        public void FillingMatrixWithTwoStartingNumbersTest()
        {
            for (int i = 0; i < 1000; i++)
            {
                //Arrange
                GameModel model = new GameModel(4);

                //Act
                model.GameField = StartEngine.FillGameFiled(model.FieldSize);
                List<int> listOfNumbers = TestUtilities.GetAllNumbersInGameFieldMatrix(model.GameField);

                //Assert
                Assert.AreEqual(2, listOfNumbers.Count(x => x != 0));
                Assert.AreNotEqual(2, listOfNumbers.Count(x => x == 4));
                Assert.IsTrue(listOfNumbers.All(x => x == 0 || x == 2 || x == 4));
            }
        }
    }
}
