﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGame2048BLL.Engine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectGame2048Tests
{
    [TestClass]
    public class MovementTests
    {
        public int[,] MatrixTest_1 { get; private set; }
        public int[,] MatrixTest_2 { get; private set; }

        [TestInitialize]
        public void InitializeMatrixes()
        {
            //Arrange
            MatrixTest_1 = new int[,]
            { 
              { 0, 2, 2, 4 },
              { 0, 0, 0, 2 },
              { 4, 16, 4, 2 },
              { 8, 8, 4, 4 }
            };

            MatrixTest_2 = new int[,]
            {
              { 2, 2, 2, 2 },
              { 0, 0, 4, 4 },
              { 32, 8, 4, 2 },
              { 4, 4, 4, 4 }
            };
        }

        [TestMethod]
        public void GenerateANewNumberTest()
        {
            for (int i = 0; i < 1000; i++)
            {
                //Arrange
                int[,] matrix = new int[4, 4];

                //Act
                Movement.GenerateNumber(matrix);
                List<int> listOfNumbers = TestUtilities.GetAllNumbersInGameFieldMatrix(matrix);

                //Assert
                Assert.AreEqual(1, listOfNumbers.Count(x => x != 0));
                Assert.IsTrue(listOfNumbers.All(x => x == 0 || x == 2 || x == 4));
            }
        }

        [TestMethod]
        public void CheckUpWithMatrixTest_1Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 4, 2, 2, 4 },
              { 8, 16, 8, 4 },
              { 0, 8, 0, 4 },
              { 0, 0, 0, 0 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(this.MatrixTest_1);
            int result = Movement.LeftOrUpMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(12, result);
        }

        [TestMethod]
        public void CheckUpSWithMatrixTest_2Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 2, 2, 2, 2 },
              { 32, 8, 8, 4 },
              { 4, 4, 4, 2 },
              { 0, 0, 0, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(this.MatrixTest_2);
            int result = Movement.LeftOrUpMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void CheckLeftWithMatrixTest_1Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 4, 4, 0, 0 },
              { 2, 0, 0, 0 },
              { 4, 16, 4, 2 },
              { 16, 8, 0, 0 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(this.MatrixTest_1);
            int result = Movement.LeftOrUpMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(28, result);
        }

        [TestMethod]
        public void CheckLeftWithMatrixTest_2Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 4, 4, 0, 0 },
              { 8, 0, 0, 0 },
              { 32, 8, 4, 2 },
              { 8, 8, 0, 0 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(this.MatrixTest_2);
            int result = Movement.LeftOrUpMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(32, result);
        }

        [TestMethod]
        public void CheckDownWithMatrixTest_1Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 0, 0, 0, 0 },
              { 0, 2, 0, 4 },
              { 4, 16, 2, 4 },
              { 8, 8, 8, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(this.MatrixTest_1);
            int result = Movement.RightOrDownMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(12, result);
        }

        [TestMethod]
        public void CheckDownWithMatrixTest_2Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 0, 0, 0, 2 },
              { 2, 2, 2, 4 },
              { 32, 8, 4, 2 },
              { 4, 4, 8, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(this.MatrixTest_2);
            int result = Movement.RightOrDownMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void CheckRightWithMatrixTest_1Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 0, 0, 4, 4 },
              { 0, 0, 0, 2 },
              { 4, 16, 4, 2 },
              { 0, 0, 16, 8 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(this.MatrixTest_1);
            int result = Movement.RightOrDownMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(28, result);
        }

        [TestMethod]
        public void CheckRightWithMatrixTest_2Test()
        {
            //Arrange
            int[,] matrixResult = new int[,]
            {
              { 0, 0, 4, 4 },
              { 0, 0, 0, 8 },
              { 32, 8, 4, 2 },
              { 0, 0, 8, 8 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(this.MatrixTest_2);
            int result = Movement.RightOrDownMovement(jArr, out bool generate);
            int[,] matrix = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixResult, matrix);
            Assert.IsTrue(generate);
            Assert.AreEqual(32, result);
        }

        [TestMethod]
        public void CheckManyUpMovesTest()
        {
            //Arranage
            int[,] matrixTest = new int[,]
            {
              { 2, 2, 2, 2 },
              { 4, 8, 8, 4 },
              { 0, 4, 16, 2 },
              { 0, 0, 0, 16 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(matrixTest);

            for (int i = 0; i < 5; i++)
            {
                Movement.LeftOrUpMovement(jArr, out bool generate);
            }

            int[,] matrixResult = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixTest, matrixResult);
        }

        [TestMethod]
        public void CheckManyLeftMovesTest()
        {
            //Arrange
            int[,] matrixTest = new int[,]
            {
                { 4, 2, 16, 0 },
                { 2, 0, 0, 0 },
                { 4, 2, 4, 2 },
                { 2, 32, 16, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(matrixTest);

            for (int i = 0; i < 5; i++)
            {
                Movement.LeftOrUpMovement(jArr, out bool generate);
            }

            int[,] matrixResult = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixTest, matrixResult);
        }

        [TestMethod]
        public void CheckManyDownMovesTest()
        {
            //Arrange
            int[,] matrixTest = new int[,]
            {
                { 4, 0, 0, 0 },
                { 2, 0, 16, 0 },
                { 4, 2, 4, 2 },
                { 2, 32, 16, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractColumnsFromMatrix(matrixTest);

            for (int i = 0; i < 5; i++)
            {
                Movement.RightOrDownMovement(jArr, out bool generate);
            }

            int[,] matrixResult = Movement.ConstructAMatrixFromArraysColumns(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixTest, matrixResult);
        }

        [TestMethod]
        public void CheckManyRightMovesTest()
        {
            //Arrange
            int[,] matrixTest = new int[,]
            {
                { 0, 0, 0, 4 },
                { 0, 0, 16, 8 },
                { 4, 2, 4, 2 },
                { 2, 32, 16, 4 }
            };

            //Act
            int[][] jArr = Movement.ExtractRowsFromMatrix(matrixTest);

            for (int i = 0; i < 5; i++)
            {
                Movement.RightOrDownMovement(jArr, out bool generate);
            }

            int[,] matrixResult = Movement.ConstructAMatrixFromArraysRows(jArr);

            //Assert
            CollectionAssert.AreEqual(matrixTest, matrixResult);
        }
    }
}
