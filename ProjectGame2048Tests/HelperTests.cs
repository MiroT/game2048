﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGame2048BLL.HelperClasses;

namespace ProjectGame2048Tests
{
    [TestClass]
    public class HelperTests
    {
        public int[,] MatrixTest { get; private set; }

        [TestInitialize]
        public void InitializeMatrix()
        {
            //Arrange
            MatrixTest = new int[,]
            {
              { 0, 2, 2, 4 },
              { 0, 0, 0, 2 },
              { 4, 16, 4, 2 },
              { 8, 8, 4, 4 }
            };
        }

        [TestMethod]
        public void ConvertMatrixToStringTests()
        {
            //Arrange
            string matrixResult = "0,2,2,4,0,0,0,2,4,16,4,2,8,8,4,4";

            //Act
            string result = Helper.ConvertMatrixToString(MatrixTest);

            //Assert
            Assert.AreEqual(matrixResult, result);
        }

        [TestMethod]
        public void CreateMatrixAndScoreFromStringTest()
        {
            //Arrange
            int[,] resultMatrix = null;
            int resultScore = 0;
            string matrixAndScoreResult = "0,2,2,4,0,0,0,2,4,16,4,2,8,8,4,4,270";

            //Act
            (resultMatrix, resultScore) = Helper.CreateMatrixAndScoreFromString(matrixAndScoreResult);

            //Assert
            Assert.AreEqual(270, resultScore);
            CollectionAssert.AreEqual(MatrixTest, resultMatrix);
        }

        [TestMethod]
        public void CreateMatrixFromStringTest()
        {
            //Arrange
            int[,] resultMatrix = null;
            string matrixAndScoreResult = "0,2,2,4,0,0,0,2,4,16,4,2,8,8,4,4,270";

            //Act
            resultMatrix = Helper.CreateMatrixFromString(matrixAndScoreResult);

            //Assert
            CollectionAssert.AreEqual(MatrixTest, resultMatrix);
        }
    }
}
