﻿using System;
using System.IO;
using System.Text;

namespace ProjectGame2048BLL.HelperClasses
{
    public static class RepositoryHelper
    {
        public static async void WriteException(Exception e)
        {
            StreamWriter sw = new StreamWriter("LogExceptions.txt", true, Encoding.UTF8);

            using (sw)
            {
                await sw.WriteLineAsync(
                    $"InnerException: {e.InnerException}; " +
                    $"Message: {e.Message}; " +
                    $"StackTrace: {e.StackTrace}"
                    );
            }
        }
    }
}
