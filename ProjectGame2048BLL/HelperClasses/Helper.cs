﻿using System.Text;

namespace ProjectGame2048BLL.HelperClasses
{
    public static class Helper
    {
        /// <summary>
        /// Converts a matrix to string
        /// </summary>
        /// <param name="matrix">the matrix to convert</param>
        /// <returns>the converted matrix</returns>
        public static string ConvertMatrixToString(int[,] matrix)
        {
            StringBuilder stringBuilder = new StringBuilder(16);

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (row == matrix.GetLength(0) - 1 && col == matrix.GetLength(1) - 1)
                    {

                        stringBuilder.Append($"{matrix[row, col]}");
                    }
                    else
                    {
                        stringBuilder.Append($"{matrix[row, col]},");
                    }
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Creates a matrix and a result of integer type from given string
        /// </summary>
        /// <param name="values">the string</param>
        /// <returns>the matrix and the score</returns>
        public static (int[,], int) CreateMatrixAndScoreFromString(string values)
        {
            string[] numbers = values.Split(',');
            int[,] matrix = new int[4, 4];
            int score = int.Parse(numbers[numbers.Length - 1]);

            for (int row = 0, counter = 0; row < 4; row++, counter += 4)
            {
                for (int col = 0; col < 4; col++)
                {
                    matrix[row, col] = int.Parse(numbers[col + counter]);
                }
            }

            return (matrix, score);
        }

        /// <summary>
        /// Creates a matrix from given string
        /// </summary>
        /// <param name="values">the string</param>
        /// <returns>the matrix</returns>
        public static int[,] CreateMatrixFromString(string values)
        {
            string[] numbers = values.Split(',');
            int[,] matrix = new int[4, 4];

            for (int row = 0, counter = 0; row < 4; row++, counter += 4)
            {
                for (int col = 0; col < 4; col++)
                {
                    matrix[row, col] = int.Parse(numbers[col + counter]);
                }
            }

            return matrix;
        }
    }
}
