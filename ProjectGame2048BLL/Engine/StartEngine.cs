﻿using System;

namespace ProjectGame2048BLL.Engine
{
    public static class StartEngine
    {
        private const int fourChance = 20;

        public static int[,] FillGameFiled(int size)
        {
            int[,] matrix = new int[size, size];
            Random random = new Random();

            int[] startingNumbers = new int[2];
            int[] coordinates = new int[4];

            //Generate 4 coordinates untill they are not the same
            do
            {
                for (int i = 0; i < 4; i++)
                {
                    coordinates[i] = random.Next() % matrix.GetLength(0);
                }
            } while (coordinates[0] == coordinates[2] && coordinates[1] == coordinates[3]);

            //Generate the starting numbers untill they are not two fours
            do
            {
                for (int i = 0; i < 2; i++)
                {
                    startingNumbers[i] = random.Next(100) > fourChance ? 2 : 4;
                }
            } while (startingNumbers[0] == 4 && startingNumbers[1] == 4);

            matrix[coordinates[0], coordinates[1]] = startingNumbers[0];
            matrix[coordinates[2], coordinates[3]] = startingNumbers[1];

            return matrix;
        }
    }
}
