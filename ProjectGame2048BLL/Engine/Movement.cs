﻿using System;
using System.Collections.Generic;

namespace ProjectGame2048BLL.Engine
{
    public static class Movement
    {
        /// <summary>
        /// Calls appropriate methods to make the move
        /// </summary>
        /// <param name="move">the move to be made</param>
        /// <param name="matrix">the matrix which is used as a game field</param>
        /// <returns>returns the matrix with the performed move</returns>
        public static int[,] PerformMove(Moves move, int[,] matrix, out bool canMove, out int result)
        {
            int[][] arr;
            int[,] gameField = new int[4, 4];
            bool canGenerateNumber = false;
            canMove = true;
            result = 0;

            switch (move)
            {
                case Moves.Right:
                    arr = ExtractRowsFromMatrix(matrix);
                    result = RightOrDownMovement(arr, out canGenerateNumber);
                    gameField = ConstructAMatrixFromArraysRows(arr);
                    break;
                case Moves.Left:
                    arr = ExtractRowsFromMatrix(matrix);
                    result = LeftOrUpMovement(arr, out canGenerateNumber);
                    gameField = ConstructAMatrixFromArraysRows(arr);
                    break;
                case Moves.Down:
                    arr = ExtractColumnsFromMatrix(matrix);
                    result = RightOrDownMovement(arr, out canGenerateNumber);
                    gameField = ConstructAMatrixFromArraysColumns(arr);
                    break;
                case Moves.Up:
                    arr = ExtractColumnsFromMatrix(matrix);
                    result = LeftOrUpMovement(arr, out canGenerateNumber);
                    gameField = ConstructAMatrixFromArraysColumns(arr);
                    break;
            }

            if (canGenerateNumber)
            {
                GenerateNumber(gameField);
            }
            else
            {
                if (!CheckForFreePosition(gameField))
                {
                    if (!CheckIfSumIsPossibe(gameField))
                    {
                        canMove = false;
                    }
                }
            }

            return gameField;
        }

        /// <summary>
        /// Calls appropriate methods to perform the move
        /// </summary>
        /// <param name="jaggedArray">the jagged array with rows or collumns</param>
        public static int LeftOrUpMovement(int[][] jaggedArray, out bool canGenerateNumber)
        {
            canGenerateNumber = false;
            int result = 0;

            for (int i = 0; i < jaggedArray.Length; i++)
            {
                result += LeftOrUpSumElements(jaggedArray[i], out bool hasSummed);
                LeftOrUpMoveElements(jaggedArray[i], out bool canMakeMove);
                if (canMakeMove || hasSummed)
                {
                    canGenerateNumber = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Sums the same elements under some conditions
        /// </summary>
        /// <param name="arr">the array with elements</param>
        private static int LeftOrUpSumElements(int[] arr, out bool hasSummed)
        {
            hasSummed = false;
            int result = 0; 
            for (int j = 0; j < arr.Length; j++)
            {
                if (arr[j] != 0)
                {
                    for (int k = j + 1; k < arr.Length; k++)
                    {
                        if (arr[k] != 0 && arr[k] != arr[j])
                        {
                            break;
                        }

                        //If they are the same sum them and make the position of the second element free
                        if (arr[k] == arr[j])
                        {
                            hasSummed = true;
                            arr[j] *= 2;
                            result += arr[j];
                            arr[k] = 0;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Moves the elements in the most left or up free position
        /// </summary>
        /// <param name="arr">the array with elements</param>
        private static void LeftOrUpMoveElements(int[] arr, out bool canMove)
        {
            List<int> freePositions = new List<int>(4);
            canMove = false;
            for (int j = 0; j < arr.Length; j++)
            {
                if (arr[j] == 0)
                {
                    freePositions.Add(j);
                }
                else
                {
                    //If it is not the first position (because the element is already on the most left position)
                    if (j != 0)
                    {
                        //Checks if there are free positions
                        if (freePositions.Count != 0)
                        {
                            canMove = true;
                            int position = freePositions[0];
                            freePositions.RemoveAt(0);

                            //Set the element to the most left free position
                            arr[position] = arr[j];
                            arr[j] = 0;
                            freePositions.Add(j);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calls appropriate methods to perform the move
        /// </summary>
        /// <param name="jaggedArray">the jagged array with rows or collumns</param>
        public static int RightOrDownMovement(int[][] jaggedArray, out bool canGenerateNumber)
        {
            canGenerateNumber = false;
            int result = 0;

            for (int i = 0; i < jaggedArray.Length; i++)
            {
                result += RightOrDownSumElements(jaggedArray[i], out bool hasSummed);
                RightOrDownMoveElements(jaggedArray[i], out bool canMakeMove);
                if (canMakeMove || hasSummed)
                {
                    canGenerateNumber = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Sums the same elements under some conditions
        /// </summary>
        /// <param name="arr">the array with elements</param>
        private static int RightOrDownSumElements(int[] arr, out bool hasSummed)
        {
            hasSummed = false;
            int result = 0;
            for (int j = arr.Length - 1; j >= 0; j--)
            {
                if (arr[j] != 0)
                {
                    for (int k = j - 1; k >= 0; k--)
                    {
                        if (arr[k] != 0 && arr[k] != arr[j])
                        {
                            break;
                        }

                        if (arr[j] == arr[k])
                        {
                            hasSummed = true;
                            //Sum the two numbers
                            arr[j] *= 2;
                            result += arr[j];
                            arr[k] = 0;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Moves the elements in the most right or down free position
        /// </summary>
        /// <param name="arr">the array with elements</param>
        private static void RightOrDownMoveElements(int[] arr, out bool canMove)
        {
            List<int> freePositions = new List<int>(4);
            canMove = false;
            for (int j = arr.Length - 1; j >= 0; j--)
            {
                if (arr[j] == 0)
                {
                    freePositions.Add(j);
                }
                else
                {
                    //If it is not the first position (because the element is already on the most right position)
                    if (j != arr.Length - 1)
                    {
                        if (freePositions.Count != 0)
                        {
                            canMove = true;
                            int position = freePositions[0];
                            freePositions.RemoveAt(0);

                            //Set the element to the most right free position
                            arr[position] = arr[j];
                            arr[j] = 0;

                            //Mark the new free position and insert it in the list as a new free position
                            freePositions.Add(j);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates an array for every column and save the result in a jagged array
        /// </summary>
        /// <param name="matrix">the matrix from which to extract the columns</param>
        /// <returns>The jagged array containing the extracted columns of the matrix</returns>
        public static int[][] ExtractColumnsFromMatrix(int[,] matrix)
        {
            int[][] extractedColls = new int[4][];

            for (int col = 0; col < matrix.GetLength(1); col++)
            {
                //Create an array for every col
                extractedColls[col] = new int[4];

                for (int row = 0; row < matrix.GetLength(0); row++)
                {
                    extractedColls[col][row] = matrix[row, col];
                }
            }

            return extractedColls;
        }

        /// <summary>
        /// Creates an array for every row and save the result in a jagged array
        /// </summary>
        /// <param name="matrix">the matrix from which to extract the rows</param>
        /// <returns>The jagged array containing the extracted rows of the matrix</returns>
        public static int[][] ExtractRowsFromMatrix(int[,] matrix)
        {
            int[][] extractedRows = new int[4][];

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                //Create an array for every row
                extractedRows[row] = new int[4];

                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    extractedRows[row][col] = matrix[row, col];
                }
            }

            return extractedRows;
        }

        /// <summary>
        /// Constructs a matrix from four given arrays in a row order
        /// </summary>
        /// <param name="arr">the four arrays as jagged array</param>
        /// <returns>returns the constructed matrix</returns>
        public static int[,] ConstructAMatrixFromArraysRows(int[][] arr)
        {
            int[,] gameField = new int[4, 4];

            for (int row = 0; row < arr.Length; row++)
            {
                for (int col = 0; col < arr[row].Length; col++)
                {
                    gameField[row, col] = arr[row][col];
                }
            }

            return gameField;
        }

        /// <summary>
        /// Constructs a matrix from four given arrays in a collumn order
        /// </summary>
        /// <param name="arr">the four arrays as jagged array</param>
        /// <returns>returns the constructed matrix</returns>
        public static int[,] ConstructAMatrixFromArraysColumns(int[][] arr)
        {
            int[,] gameField = new int[4, 4];

            for (int col = 0; col < arr[0].Length; col++)
            {
                for (int row = 0; row < arr.Length; row++)
                {
                    gameField[col, row] = arr[row][col];
                }
            }

            return gameField;
        }

        /// <summary>
        /// Contains two coordinates X and Y
        /// </summary>
        private struct Coordinates
        {
            public int X { get; set; }
            public int Y { get; set; }
        }

        /// <summary>
        /// Generates a new number in the gine matrix
        /// </summary>
        /// <param name="matrix">the matrix</param>
        public static void GenerateNumber(int[,] matrix)
        {
            Random random = new Random();
            int number = random.Next(100) > 20 ? 2 : 4;
            List<Coordinates> freePositions = new List<Coordinates>();

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (matrix[row, col] == 0)
                    {
                        freePositions.Add(new Coordinates { X = row, Y = col });
                    }
                }
            }

            int position = random.Next() % freePositions.Count;
            matrix[freePositions[position].X, freePositions[position].Y] = number;
        }

        /// <summary>
        /// Checks if there are possible sums in the matrix
        /// </summary>
        /// <param name="matrix">the matrix to check</param>
        /// <returns>if there are possible sums</returns>
        public static bool CheckIfSumIsPossibe(int[,] matrix)
        {
            bool ifSumsInInnerMatrix = CheckMiddleMatrixForSums(matrix);
            bool ifSumsInEndRows = CheckEndRowsForSums(matrix);
            bool ifSumsInEndCollumns = CheckEndCollumnsForSums(matrix);

            if (ifSumsInInnerMatrix)
            {
                return true;
            }
            else if(ifSumsInEndRows)
            {
                return true;
            }
            else if(ifSumsInEndCollumns)
            {
                return true;
            }

            return false;

            //if (ifSumsInInnerMatrix || ifSumsInEndRows || ifSumsInEndCollumns)
            //{
            //    return true;
            //}

            //return false;
        }

        /// <summary>
        /// Checks the end collumns in a matrix for possible sums
        /// </summary>
        /// <param name="matrix">the matrix to check</param>
        /// <returns>if there are possible sums</returns>
        private static bool CheckEndCollumnsForSums(int[,] matrix)
        {
            for (int col = 0; col < 4; col += 3)
            {
                for (int row = 1; row <= 2; row++)
                {
                    if (matrix[row, col] == matrix[row - 1, col])
                    {
                        return true;
                    }

                    if (matrix[row, col] == matrix[row + 1, col])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks the end rows in a matrix for possible sums
        /// </summary>
        /// <param name="matrix">the matrix to check</param>
        /// <returns>if there are possible sums</returns>
        private static bool CheckEndRowsForSums(int[,] matrix)
        {
            for (int row = 0; row < 4; row += 3)
            {
                for (int col = 1; col <= 2; col++)
                {
                    if (matrix[row, col] == matrix[row, col - 1])
                    {
                        return true;
                    }

                    if (matrix[row, col] == matrix[row, col + 1])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks the inner matrix for possible sums
        /// </summary>
        /// <param name="matrix">the matrix to check</param>
        /// <returns>if there are possible sums</returns>
        private static bool CheckMiddleMatrixForSums(int[,] matrix)
        {
            for (int row = 1; row <= 2; row++)
            {
                for (int col = 1; col <= 2; col++)
                {
                    if (matrix[row, col] == matrix[row, col + 1])
                    {
                        return true;
                    }

                    if (matrix[row, col] == matrix[row, col - 1])
                    {
                        return true;
                    }

                    if (matrix[row, col] == matrix[row + 1, col])
                    {
                        return true;
                    }

                    if (matrix[row, col] == matrix[row - 1, col])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if there is a free position in the matrix
        /// </summary>
        /// <param name="matrix">the matrix</param>
        /// <returns>boolean if there is a free position</returns>
        public static bool CheckForFreePosition(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (matrix[row, col] == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}