﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectGame2048.Migrations
{
    public partial class AddedRecordField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Record",
                table: "GameModels",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Record",
                table: "GameModels");
        }
    }
}
