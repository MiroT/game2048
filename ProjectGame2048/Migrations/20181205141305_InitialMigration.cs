﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectGame2048.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GameModels",
                columns: table => new
                {
                    DatabaseModelId = table.Column<Guid>(nullable: false),
                    GameField = table.Column<string>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameModels", x => x.DatabaseModelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameModels");
        }
    }
}
