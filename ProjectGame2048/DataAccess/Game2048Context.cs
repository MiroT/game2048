﻿using Microsoft.EntityFrameworkCore;
using ProjectGame2048.Models;

namespace ProjectGame2048.DataAccess
{
    public class Game2048Context : DbContext
    {
        public Game2048Context(DbContextOptions<Game2048Context> options)
            : base(options)
        { 
        }

        public DbSet<DatabaseModel> GameModels { get; set; }
    }
}
