﻿$(document).ready(function () {
    $(".button").click(function () {
        var val = $(this).val();
        var url = '/Game/Move/' + val;

        $.ajax({
            url: url,
            type: 'POST',
            success: function (result) {
                $("#partial").html(result);
            }
        });
    });

    $(document).keydown(function (event) {
        var url = '/Game/Move/';

        switch (event.which) {
            case 37:
                url += "Left";
                event.preventDefault();
                break;
            case 38:
                url += "Up";
                event.preventDefault();
                break;
            case 39:
                url += "Right";
                event.preventDefault();
                break;
            case 40:
                url += "Down";
                event.preventDefault();
                break;
        }

        $.ajax({
            url: url,
            type: 'POST',
            success: function (result) {
                $("#partial").html(result);
            }
        });
    });

    $("#newGame").click(function () {
        $.ajax({
            url: '/Game/CreateNewGame',
            type: 'GET',
            success: function (result) {
                $("#partial").html(result);
            }
        });
    });

    $("#saveGame").click(function () {
        $.ajax({
            url: '/Game/SaveGame',
            type: 'GET',
            success: function (result) {
                $("#partial").html(result);
            }
        });
    });

    $("#loadGame").click(function () {
        $.ajax({
            url: '/Game/LoadGame',
            type: 'GET',
            success: function (result) {
                $("#partial").html(result);
            }
        });
    });
});