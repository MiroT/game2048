﻿namespace ProjectGame2048.Models
{
    public class GameModel
    {
        public int[,] GameField { get; set; }

        public int FieldSize { get; private set; }

        public GameModel(int fieldSize)
        {
            this.FieldSize = fieldSize;
            GameField = new int[this.FieldSize, this.FieldSize];
        }
    }
}
