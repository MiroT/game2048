﻿using System;

namespace ProjectGame2048.Models
{
    public class DatabaseModel
    {
        public Guid DatabaseModelId { get; set; }

        public string GameField { get; set; }

        public int Score { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Record { get; set; }
    }
}
