﻿using Microsoft.EntityFrameworkCore;
using ProjectGame2048.DataAccess;
using ProjectGame2048.Models;
using ProjectGame2048BLL.HelperClasses;
using System;

namespace ProjectGame2048.Repository
{
    public class GameRepository
    {
        private readonly Game2048Context context;
        private DbSet<DatabaseModel> dbSet;

        public GameRepository(Game2048Context context)
        {
            this.context = context;
            this.dbSet = context.Set<DatabaseModel>();
        }

        public DatabaseModel Get(string id)
        {
            return dbSet.Find(new Guid(id));
        }

        public void Insert(DatabaseModel model)
        {
            context.Entry(model).State = EntityState.Added;
            Save();
            Dispose();
        }

        public void Update(DatabaseModel model)
        {
            context.Entry(model).State = EntityState.Modified;
            Save();
            Dispose();
        }

        public void Delete(DatabaseModel model)
        {
            context.Entry(model).State = EntityState.Deleted;
            Save();
            Dispose();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }

            disposed = true;
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                RepositoryHelper.WriteException(e);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
