﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectGame2048.DataAccess;
using ProjectGame2048.Models;
using ProjectGame2048.Repository;
using ProjectGame2048BLL.Engine;
using ProjectGame2048BLL.HelperClasses;
using System;

namespace ProjectGame2048.Controllers
{
    public class GameController : Controller
    {
        private const string CookieKey = "matrix";
        private readonly GameRepository repository;

        public GameController(Game2048Context context)
        {
            repository = new GameRepository(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            GameModel model = new GameModel(4);
            string value = HttpContext.Session.GetString("Matrix");
            string cookie = GetCookie(CookieKey);

            if (value != null)
            {
                int[,] matrix = Helper.CreateMatrixFromString(value);
                int score = (int)HttpContext.Session.GetInt32("Score");
                model.GameField = matrix;
                ViewBag.Result = score;
                int record = (int)HttpContext.Session.GetInt32("Record");
                ViewBag.Record = record;
            }
            else if(cookie != null)
            {
                int[,] matrix = null;
                int score = 0;
                string record = GetCookie("record");
                (matrix, score) = Helper.CreateMatrixAndScoreFromString(cookie);
                HttpContext.Session.SetInt32("Score", score);

                string currentMatrix = Helper.ConvertMatrixToString(matrix);
                HttpContext.Session.SetString("Matrix", currentMatrix);

                if (record == null)
                {
                    HttpContext.Session.SetInt32("Record", score);
                    ViewBag.Record = score;
                }
                else
                {
                    HttpContext.Session.SetInt32("Record", int.Parse(record));
                    ViewBag.Record = record;
                }

                model.GameField = matrix;
                ViewBag.Result = score;
            }
            else
            {
                SetDefaultValues(model);
                ViewBag.Record = 0;
                HttpContext.Session.SetInt32("Record", 0);
            }

            return View(model);
        }

        [HttpPost]
        [Route("Game/Move/{move}")]
        public IActionResult Move(Moves move)
        {
            string value = HttpContext.Session.GetString("Matrix");
            int[,] currentMatrix = Helper.CreateMatrixFromString(value);
            int[,] newMatrix = Movement.PerformMove(move, currentMatrix, out bool canMove, out int currentResult);
            string strMatrix = Helper.ConvertMatrixToString(newMatrix);
            HttpContext.Session.SetString("Matrix", strMatrix);

            int score = (int)HttpContext.Session.GetInt32("Score");
            int scoreResult = score + currentResult;
            HttpContext.Session.SetInt32("Score", scoreResult);

            DeleteCookie(CookieKey);
            string cookieResult = $"{strMatrix},{scoreResult.ToString()}";
            CreateCookie(CookieKey, cookieResult);

            int record = (int)HttpContext.Session.GetInt32("Record");

            if (record <= scoreResult)
            {
                HttpContext.Session.SetInt32("Record", scoreResult);
                record = scoreResult;
                DeleteCookie("record");
                CreateCookie("record", record.ToString());
            }

            ViewBag.Record = record;

            if (!canMove)
            {
                ViewBag.GameOver = "Game Over";
            }

            ViewBag.Result = scoreResult;

            return PartialView("GameTable", newMatrix);
        }

        [HttpGet]
        public IActionResult CreateNewGame()
        {
            GameModel gameModel = new GameModel(4);

            SetDefaultValues(gameModel);

            string cookie = GetCookie("record");
            if (cookie != null)
            {
                int record = int.Parse(cookie);
                HttpContext.Session.SetInt32("Record", record);
                ViewBag.Record = record;
            }
            else
            {
                HttpContext.Session.SetInt32("Record", 0);
                ViewBag.Record = 0;
            }

            return PartialView("GameTable", gameModel.GameField);
        }

        [HttpGet]
        public IActionResult LoadGame()
        {
            string id = GetCookie("UserID");
            ViewBag.Record = HttpContext.Session.GetInt32("Record");
            int[,] matrix = null;

            if (id == null)
            {
                ViewBag.Result = HttpContext.Session.GetInt32("Score");
                string strMatrix = HttpContext.Session.GetString("Matrix");
                matrix = Helper.CreateMatrixFromString(strMatrix);
                return PartialView("GameTable", matrix);
            }

            DatabaseModel model = repository.Get(id);
            int score = model.Score;
            string value = model.GameField;
            int dbRecord = model.Record;

            string currentRecord = GetCookie("record");

            if (currentRecord != null)
            {
                if (dbRecord > int.Parse(currentRecord))
                {
                    SetRecord();
                }
            }
            else
            {
                SetRecord();
            }

            void SetRecord()
            {
                DeleteCookie("record");
                HttpContext.Session.SetInt32("Record", dbRecord);
                ViewBag.Record = dbRecord;
                CreateCookie("record", dbRecord.ToString());
            }

            DeleteCookie(CookieKey);
            string cookieResult = $"{value},{score.ToString()}";
            CreateCookie(CookieKey, cookieResult);

            HttpContext.Session.SetString("Matrix", value);
            HttpContext.Session.SetInt32("Score", score);
            ViewBag.Result = score;

            matrix = Helper.CreateMatrixFromString(value);
            return PartialView("GameTable", matrix);
        }

        [HttpGet]
        public IActionResult SaveGame()
        {
            int score = (int)HttpContext.Session.GetInt32("Score");
            string value = HttpContext.Session.GetString("Matrix");
            int record = (int)HttpContext.Session.GetInt32("Record");

            string cookie = GetCookie("UserID");

            if (cookie != null)
            {
                DatabaseModel model = repository.Get(cookie);
                model.CreateDate = DateTime.Now;
                model.EndDate = DateTime.Now.AddDays(1);
                model.GameField = value;
                model.Score = score;

                if (model.Record < record)
                {
                    model.Record = record;
                }

                repository.Update(model);
            }
            else
            {
                DatabaseModel dbModel = new DatabaseModel()
                {
                    DatabaseModelId = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    EndDate = DateTime.Now.AddHours(1),
                    GameField = value,
                    Score = score,
                    Record = record
                };

                string id = dbModel.DatabaseModelId.ToString();
                CreateCookie("UserID", id);

                repository.Insert(dbModel);
            }

            ViewBag.Result = score;
            ViewBag.Record = record;

            int[,] matrix = Helper.CreateMatrixFromString(value);
            return PartialView("GameTable", matrix);
        }

        /// <summary>
        /// Creates a new game model and sets default values
        /// </summary>
        /// <returns>the model</returns>
        private void SetDefaultValues(GameModel model)
        {
            model.GameField = StartEngine.FillGameFiled(model.FieldSize);
            string matrix = Helper.ConvertMatrixToString(model.GameField);
            HttpContext.Session.SetString("Matrix", matrix);
            ViewBag.Result = 0;
            HttpContext.Session.SetInt32("Score", 0);
        }

        /// <summary>
        /// Creates a new cookie based on the value
        /// </summary>
        /// <param name="value">the value for the cookie</param>
        private void CreateCookie(string key, string value)
        {
            CookieOptions cookie = new CookieOptions();

            if (key == "UserID")
            {
                cookie.Expires = DateTime.Now.AddDays(1);
            }
            else
            {
                cookie.Expires = DateTime.Now.AddHours(1);
            }

            Response.Cookies.Append(key, value, cookie);
        }

        /// <summary>
        /// Deletes a cookie
        /// </summary>
        private void DeleteCookie(string key)
        {
            Response.Cookies.Delete(key);
        }

        /// <summary>
        /// Gets a cookie if it exists
        /// </summary>
        /// <returns>cookie's value</returns>
        private string GetCookie(string key)
        {
            string cookie;

            try
            {
                cookie = Request.Cookies[key].ToString();
            }
            catch (NullReferenceException)
            {
                return null;
            }

            return cookie;
        }
    }
}